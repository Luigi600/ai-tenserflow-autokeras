FROM jupyter/tensorflow-notebook:2023-01-04

RUN pip install -U jupyter-book
COPY --chown=${NB_UID}:${NB_GID} build-jupyter.sh /home/jovyan/
COPY --chown=${NB_UID}:${NB_GID} requirements.txt /tmp/
RUN chmod +x /home/jovyan/build-jupyter.sh
RUN pip install --quiet --no-cache-dir --requirement /tmp/requirements.txt
# idk why...
RUN pip install --quiet --upgrade jsonschema

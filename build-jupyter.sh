#!/bin/bash

for file in `ls $1/*.ipynb`
do
  jupyter-book build "$file" --path-output ./build2
done
